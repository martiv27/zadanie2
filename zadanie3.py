import math


# funkcja pobierajaca imie


def check_names():
    user_name = input("Give your name ")

    if user_name == "Marcin":
        print("Hello, ", user_name)
    else:
        print("Hello")


# Klasa rownolegloboku obliczajaca pole i porownowujaca obiekty
class Parallelogram:

    def __init__(self, a: float, h: float):
        self.a = a
        self.h = h

        self.area = a * h

    def __eq__(self, other):
        if isinstance(Parallelogram, other):
            return self.a == other.a and self.h == other.h

        elif isinstance(float, other):
            return self.area == other


# klasa dla bryly

class Cylinder:

    def __init__(self, r: float, h: float):
        self.r = r
        self.h = h
        self.volume = math.pi * r * r * h

    def __eq__(self, other):
        if isinstance(Cylinder, other):
            return self.volume == other.volume

        elif isinstance(float, other):
            return self.volume == other


# Drzewo 6 klas
class Vehicle:

    def __init__(self, price: float):
        self.price = price

    def __eq__(self, other):
        if isinstance(Vehicle, other):
            return self.price == other.price


class Boat(Vehicle):

    def __init__(self, size: float, price: float):
        super().__init__(price)
        self.size = size

    def __eq__(self, other):
        return self.size == other.size and super.__eq__(self, other)


class MasterCraft(Boat):

    def __init__(self, size: float, price: float):
        super().__init__(size, price)


class Car(Vehicle):

    def __init__(self, type: str, price: float):
        super().__init__(price)
        self.type = type

    def __eq__(self, other):
        return self.type == other.type and super.__eq__(self, other)


class Porshe(Car):

    def __init__(self, type: str, price: float):
        super().__init__(type, price)


class Toyota(Car):

    def __init__(self, type: str, price: float):
        super().__init__(type, price)
