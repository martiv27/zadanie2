#dziedziczenie klasy
class Bmw:
    def __init__(self):
        self.x = 0
        self.y = 0
class Cars(Bmw):

    def __init__(self):
        super().__init__()

#dziedziczenie metody
class Dogs:

    def __init__(self, name: str):
        self.name = name
    def stroke_dog(self):
        print(f"Glaskam psa { self.name }")

    class Pomeranian(Dogs):

        def __init__(self):
            super().__init__("Pomerianian")

#klasa ktora przeciaza metode klasy nadrzednej i uzywa jej za pomoca slowa kluczowego super
class Car:

    def __init__(self):
        pass
    def drive(self):
        print("Car is driving")
    def driving(self):
        super().drive()
