#getting name of the users

first_name = input("Give your name...")
last_name = input("Give your last name")
age = input("Give your age")

print(f"{name} {last_name} is {age} years old")
#converting input from string to float

num = input("Give a number ")

def str_to_float(number: str) -> float:
    if not number.isnumeric():
        return -1
    a = float(number)
    return a

print(str_to_float(num))
#class that takes parameters

class Daisy:

    def __init__(self, color: str, price: float, is_big: bool):
        self.color = color
        self.price = price
        self.is_big = is_big

